import React from 'react';

export  class Footer extends React.Component{
    render() {
        return (

            <div>
                <footer class="page-footer font-small unique-color-dark pt-4">
                <div class="container">

                    <ul class="list-unstyled list-inline text-left py-2">
                    <div class="list-inline-item">
                         <h5 class="mb-1">Quem somos</h5>
                        <li ><a href="#!" class="btn btn-outline-white btn-rounded"> SOBRE NÓS </a></li>
                        <li> <a href="#!" class="btn btn-outline-white btn-rounded"> CONTATO </a> </li>
                        <li> <a href="#!" class="btn btn-outline-white btn-rounded"> POLÍTICA DA EMPREGÔ </a> </li>
                        <li> <a href="#!" class="btn btn-outline-white btn-rounded"> DÚVIDAS </a> </li>



                    </div>
                    </ul>

                </div>

                <div class="footer-copyright text-center py-3">© 2020 Copyright:
                      <a href=""> Empregô </a>
                </div>

                </footer>
             </div>
        )
    }
}
export default Footer
