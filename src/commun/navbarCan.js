import React from 'react'
import logo from '../img/novaLogo.png';
import { NavLink } from 'react-router-dom';
import { FiSearch } from 'react-icons/fi';
export default class NavbarCab extends React.Component {
    render() {
        return (
            <div>
                <nav class="navbar navbar-expand-lg navbar-light py-4">
                    <a class="navbar-brand" href="/">
                        <img src={logo} alt=""></img>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse mx-auto w-auto justify-content-center" id="conteudoNavbarSuportado">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <NavLink to="/" class="nav-link" exact>Página Inicial</NavLink>
                            </li>
                        </ul>
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Meu perfil
                                 </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <a class="dropdown-item" href="#">Criar Curriculo</a>
                                    <a class="dropdown-item" href="#">Lista de Curriculos</a>
                                    <a class="dropdown-item" href="#">Vagas Candidatas</a>
                                    <a class="dropdown-item" href="#">Editar Perfil</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Sair</a>
                                </div>
                            </div>
                        </div>
                    </div>



                <div class="form-group my-2 my-lg-2">
                    <div class="input-group">

                        <input class="form-control border-right-0" placeholder="pesquisar" />
                        <span class="input-group-append bg-white border-left-0">
                            <span class="input-group-text bg-transparent">
                                <FiSearch></FiSearch>
                            </span>
                        </span>
                    </div>


                </div>

                </nav>
            </div >
        )
    }
}