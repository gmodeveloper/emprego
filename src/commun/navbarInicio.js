import React from 'react'
import logo from '../img/novaLogo.png';
import { FiSearch } from 'react-icons/fi';
export default class Navbar extends React.Component {
    render() {
        return (
            <div>
                <nav class="navbar navbar-expand-lg navbar-light bg-light py-4">
                    <a class="navbar-brand" href="#">
                        <img src={logo} alt=""></img>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse mx-auto w-auto justify-content-center" id="conteudoNavbarSuportado">
                        <ul class="navbar-nav ">
                            <li class="nav-item-active">
                                <a button type="button" class="btn"  href="/">Pagina Inicial</a>
                            </li>
                            <li class="nav-item-active">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Login" data-dismiss="modal">
                                Entrar</button>
                            </li>
                        </ul>
                    </div>
                        
                    <div class="form-group my-2 my-lg-2">
                        <div class="input-group">
                            <input class="form-control border-right-0" />
                            <span class="input-group-append bg-white border-left-0">
                                <span class="input-group-text bg-transparent">
                                        <FiSearch></FiSearch>
                                </span>
                             </span>
                        </div>
                    </div>
                    
                </nav>
            </div>
            

        )
    }
}