import React from 'react';
import Breadcrumb3 from '../commun/breadcrumb3';


export default class ListaDeCandidatos extends React.Component {
    render() {
        return (


            <div>
                <Breadcrumb3></Breadcrumb3>


                <table class="table table-hover">
                    <thead>
                        <th scope="col"> Selecionar </th>
                        <th scope="col"> Candidato </th>
                        <th scope="col"> Habilidades compatíveis </th>
                        <th scope="col"> Currículo </th>
                    </thead>

                    <tbody>
                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> José Barbosa Lima </td>
                            <td> UX, UI, HTML, CSS, JavaScipt </td>
                            <td><button type="button" class="btn btn-link" data-toggle="modal" data-target="#curriculo" data-dismiss="modal"> Ver currículo</button></td>


                        </tr>

                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> Maria Antonia Amorim da Silva </td>
                            <td> UX, UI, HTML, CSS </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>

                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> Júlia Sousa Lopes </td>
                            <td> UX, UI, HTML, CSS </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>

                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> Maria Sérgio Rodrigues Araújo Lima </td>
                            <td> UX, UI </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>

                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> Maria Luísa Costa Oliveira </td>
                            <td> UX, UI </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>

                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> Eduardo Reis Queiroz </td>
                            <td> UX, UI </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>



                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> José Barbosa Lima </td>
                            <td> UX, UI, HTML, CSS, JavaScipt </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>

                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> Maria Antonia Amorim da Silva </td>
                            <td> UX, UI, HTML, CSS </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>

                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> Júlia Sousa Lopes </td>
                            <td> UX, UI, HTML, CSS </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>

                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> Maria Sérgio Rodrigues Araújo Lima </td>
                            <td> UX, UI </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>

                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> Maria Luísa Costa Oliveira </td>
                            <td> UX, UI </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>

                        <tr>
                            <th scope="row">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            </th>
                            <td> Eduardo Reis Queiroz </td>
                            <td> UX, UI </td>
                            <td><button type="button" class="btn btn-link"> Ver currículo </button></td>
                        </tr>


                    </tbody>
                </table>


                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>

                    <button type="button" class="btn btn-primary m-5 float-right"> Solicitar entrevista </button>
                    </ul>

                </nav>
                


 
            </div>


        )
    }
}