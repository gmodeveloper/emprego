import React from 'react';
import Breadcrumb from '../commun/breadcrumb';
import Salario from '../img/icone_salario.png';

export default class cadastroVaga extends React.Component {
    render() {
        return (

            <div>

                <Breadcrumb></Breadcrumb>
                <div class="container p-0">
                    <div class="row">
                        <div class="col-6">

                            <br></br>
                            <h5 class="color font-weight-bold"> Informações da Vaga</h5><br></br>
                            <div class="form-row">
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Categoria</label>
                                    <select id="inputState" class="form-control">
                                        <option selected>...</option>
                                        <option>...</option>
                                    </select>
                                </div>

                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Profissão</label>
                                    <select id="inputState" class="form-control">
                                        <option selected>...</option>
                                        <option>...</option>
                                    </select>
                                </div>

                            </div>

                            <br></br>
                            <div class="form-group">
                                <label class="color" for="exampleFormControlTextarea1">Descrição da Vaga</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>

                            <br></br>
                            <div class="form-row">
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Salário</label>
                                    {/* <img class="salario" src={Salario} /> */}
                                    <input type="text" class="form-control" />

                                </div>
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Horário de Trabalho</label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>

                            <br></br>
                            <div class="form-row">
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Dias da Semana</label>
                                    <select id="inputState" class="form-control w-50">
                                        <option selected> Segunda </option>
                                        <option> Terça </option>
                                        <option> Quarta </option>
                                        <option> Quinta </option>
                                        <option> Sexta </option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group pt-5">
                                <h5 class="color font-weight-bold"> Requisitos do Candidato</h5> <hr></hr>
                                <h5 class="color"> Habilidades e competências</h5>
                                <br></br>
                                <label class="color2" for="exampleFormControlTextarea1">Conhecimentos do Setor</label>
                                <select id="inputState" class="form-control">
                                    <option selected> Clique para selecionar </option>
                                    <option> Administração de Banco de Dados </option>
                                    <option> AJAX </option>
                                    <option> Análise de Dados </option>
                                    <option> Animação </option>
                                    <option> Arquitetura de Informação </option>
                                </select>

                                <br></br>
                                <label class="color2" for="exampleFormControlTextarea1">Ferramentas e Tecnologias</label>
                                <select id="inputState" class="form-control">
                                    <option selected> Clique para selecionar </option>
                                    <option> ...</option>
                                    <option> ...</option>

                                </select>

                                <br></br>
                                <label class="color2" for="exampleFormControlTextarea1">competências Interpessoais</label>
                                <select id="inputState" class="form-control">
                                    <option selected> Clique para selecionar </option>
                                    <option> ...</option>
                                    <option> ...</option>

                                </select>

                            </div>

                            <br></br>
                            <div class="form-group"></div>
                            <label class="color" for="exampleFormControlTextarea1">O que você espera dos candidatos a esta vaga?</label> 
                            <small id="emailHelp" class="form-text text-muted float-right"> Opcional </small>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>


                            <br></br>
                            <div class="form-group">
                                <label class="color" for="exampleInputEmail1">Quantas vagas você deseja ofertar?</label>
                                <input type="text" class="form-control w-25"  id="exampleInputText"  aria-describedby="text" placeholder="" />
                                <small id="text" class="form-text text-muted"></small>
                            </div>

                            <br></br>
                            <button type="button" class="btn btn-primary float-right">PUBLICAR VAGA</button>

                        </div>



                    </div>
                </div>

                <br></br><br></br>
            </div>


        )
    }
}

