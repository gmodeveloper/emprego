import React from 'react';
import Image23 from '../img/image23.png';

export default class TelaCandidato extends React.Component {
    render() {
        return (
            <div>
                    <div class="how-works-candidato">  
                            <p className="titulo1">E COMO FUNCIONA O PROCESSO PARA OS CANDIDATOS?</p>  
                            <div className="howWorks-content">
                                <div className="left-howWorks">
                                    <p className="text">• Você se cadastra no site;</p>
                                    <p className="text">• Pesquisa pela vaga que deseja;</p>
                                    <p className="text">• Se candidata e preenche o currículo.</p>
                                    <p className="text-bold" >Prontinho!</p>
                                    <p className="text">Agora é só aguardar.</p>
                                </div>
                                <div className="howWorks-separator"></div>
                                <div className="right-howWorks">
                                <img className="img-candidato" src={Image23} width="500px" height="330px" alt="Responsive image" alt=""/>
                                </div>
                            </div> 
                    </div>
                </div>               

                 
        )
    }
}