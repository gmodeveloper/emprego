import React from 'react';
import Icone1 from '../img/icone  (9).png';
import Icone2 from '../img/icone  (7).png';
import Icone3 from '../img/icone  (10).png';
import Icone4 from '../img/icone  (6).png';
import Icone5 from '../img/icone  (1).png';
import Icone6 from '../img/icone  (8).png';
import Carousel1 from '../img/Carousel1.jpg';
import Carousel2 from '../img/Carousel2.jpg';
import Carousel3 from '../img/Carousel3.jpg';
import Card1 from '../img/icone  (5).png';
import Card2 from '../img/icone  (3).png';
import Card3 from '../img/icone  (4).png';
import TelaEmpresa from './telaEmpresa';
import TelaCandidato from './telaCandidato';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tipo: props.tipo
        }
    }
    render() {
        return (
            <div className="TelaInicial">
                <div className="content-site">
                    <nav class="navbar navbar-light">
                        <a class="navbar-brand" href="#">
                            <img src={Icone1} width="50" height="50" class="d-inline-block align-top" alt=""></img>
                            <br />
                            <span class="Categoria">Tecnologia</span>
                        </a>
                        <a class="navbar-brand " href="#">
                            <img src={Icone2} width="50" height="50" class="d-inline-block align-top" alt=""></img>
                            <br />
                            <span class="Categoria">Educação</span>
                        </a>
                        <a class="navbar-brand" href="#">
                            <img src={Icone3} width="50" height="50" class="d-inline-block align-top" alt=""></img>
                            <br />
                            <span class="Categoria">Consultoria</span>
                        </a>
                        <a class="navbar-brand" href="#">
                            <img src={Icone4} width="50" height="50" class="d-inline-block align-top" alt=""></img>
                            <br />
                            <span class="Categoria">Finanças</span>
                        </a>
                        <a class="navbar-brand " href="#">
                            <img src={Icone5} width="50" height="50" class="d-inline-block align-top" alt=""></img>
                            <br />
                            <span class="Categoria">Transportes</span>
                        </a>
                        <a class="navbar-brand" href="#">
                            <img src={Icone6} width="50" height="50" class="d-inline-block align-top" alt=""></img>
                            <br />
                            <span class="Categoria">Joalheria</span>
                        </a>
                    </nav>

                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src={Carousel1} class="d-block w-100" alt="..." alt=""/>
                                </div>
                                <div class="carousel-item">
                                    <img src={Carousel2} class="d-block w-100" alt="..." alt=""/>
                                    </div>
                                    <div class="carousel-item">
                                        <img src={Carousel3} class="d-block w-100" alt="..." alt=""/>
                                    </div>
                                    </div>
                                    <a class="carousel-control-prev azulClaro" href="#carouselExampleControls" role="button" data-slide="prev">
                                        <span class="" aria-hidden="true">
                                        &lsaquo;
                                        </span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next azulClaro" href="#carouselExampleControls" role="button" data-slide="next">
                                        <span class="" aria-hidden="true">
                                            &rsaquo;
                                        </span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>

                                <div className="cards-Telainicial">
                                    <div className="card-content">
                                        <div className="card-item">
                                            <img class="d-block-center" x="20%" y="20%" src={Card1} alt="Primeiro Slide" width="250" />
                                            <div className="card-separador1"></div>
                                            <h2 class="card-title" type="button"><p></p>Confiança</h2>
                                        </div>

                                        <div className="card-item">
                                            <img class="d-block-center" x="20%" y="20%" src={Card2} alt="Primeiro Slide" width="220" />
                                            <div className="card-separador"></div>
                                            <h2 class="card-title" type="button"><p></p>Conectividade</h2>
                                        </div>

                                        <div className="card-item">
                                            <img class="d-block-center" x="20%" y="20%" src={Card3} alt="Primeiro Slide" width="220" />
                                            <div className="card-separador2"></div>
                                            <h2 class="card-title" type="button"><p></p>Fluidez</h2>
                                        </div>
                                    </div>
                                </div>
                                <TelaEmpresa></TelaEmpresa>
                                <TelaCandidato></TelaCandidato>
                            </div>
                        </div>

        )

    }

}

