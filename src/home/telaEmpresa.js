import React from 'react';
import Image24 from '../img/image24.png';

export default class TelaEmpresa extends React.Component {
    render() {
        return (
            <div>
                    <div class="how-works-empresa">  
                            <p className="titulo1">E COMO FUNCIONA O PROCESSO PARA AS EMPRESAS?</p>  
                            <div className="howWorks-content">
                                <div className="left-howWorks">
                                    <p className="text">• Você se cadastra no site;</p>
                                    <p className="text">• Vai em "Publicar Vaga";</p>
                                    <p className="text">• Preenche os requisitos da vaga e pública</p>
                                    <p className="text-bold">Prontinho!</p>
                                    <p className="text">Agora é só aguardar.</p>
                                </div>
                                <div className="howWorks-separator"></div>
                                <div className="right-howWorks">
                                <img src={Image24} width="500px" height="330px" alt="Responsive image"  alt=""/>
                                </div>
                            </div> 
                    </div>
                </div>               

                 
        )
    }
}