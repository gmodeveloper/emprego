import React from 'react';

export default class Footer extends React.Component {
    render() {
        return (        
        
            <footer class="footer-copyright text-center py-3">
                <div class="container text-center text-md-left">
                        <h6 class="text-uppercase mb-4 font-weight-bold">QUEM SOMOS?</h6>
                        <p>
                        <a href="#!">SOBRE NÓS</a>
                        </p>
                        <p>
                        <a href="#!">CONTATOS</a>
                        </p>
                        <p>
                        <a href="#!">DÚVIDAS</a>
                        </p>
                        <p>
                        <a href="#!">PÓLITICAS DA EMPREGÔ</a>
                        </p>
                </div>
                <div class="text-center py-3">© 2020 EMPREGÔ</div>   

            </footer>

        )
    } 

}   