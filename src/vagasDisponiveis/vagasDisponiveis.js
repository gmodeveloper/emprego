import React from 'react';
import Breadcrumb from '../commun/breadcrumb';
import LogoHF from '../img/image1.png';

export default class VagasDisponiveis extends React.Component {

    render() {
        return (


            <div>
                <Breadcrumb></Breadcrumb>
                <div class="container bg-light p-0 w-25 ml-5">
                    <div class="row">
                        <div class="col-6"></div>
                        <div class="card">
                            <img class="img-fluid m-3 m-auto" src={LogoHF} />

                            <div class="card-body">
                                <h5 class="card-title text-center">Técnico em TI</h5>
                                <hr></hr>
                                <p class="card-text">Para programador (a) full-stack</p>
                                <a href="#" class="btn btn-primary">Candidatar</a>
                                <a href="#" class="btn btn-outline-primary">Ver vaga</a>

                            </div>
                        </div>
                    </div>

                </div>

    

            </div>



        )
    }
}