import React from 'react';
import Breadcrumb2 from '../commun/breadcrumb2';

export default class cadastroCurriculo extends React.Component {

    render() {
        return (

            <div>

                <Breadcrumb2></Breadcrumb2>
                <div class="container bg-white p-0">
                    <div class="row">
                        <div class="col-6">
                            <br></br>
                            <h5 class="color font-weight-bold"> Formação Profissional </h5>
                            <hr></hr>
                            <div class="form-row">
                                <div class="col">
                                    <label class="color" for="exampleInput">Grau de escolaridade</label>
                                    <input type="" class="form-control" id="exampleInput" aria-describedby="emailHelp" />
                                    <small id="emailHelp" class="form-text text-muted">Campo obrigatório.</small>
                                </div>
                            </div>

                            <br></br>
                            <div class="form-row">
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Nome do curso</label>
                                    <input type="text" class="form-control w-50" />
                                </div>
                            </div>

                            <br></br>
                            <div class="form-row">
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Nome da Instituição</label>
                                    <input type="text" class="form-control" />
                                </div>
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Ano de conclusão</label>
                                    <input type="text" class="form-control" />
                                    <br></br>
                                    <button type="button" class="btn btn-outline-primary float-right"> <strong> + Adicionar </strong> </button>

                                </div>
                            </div>


                            <br></br><br></br><br></br>
                            <form>
                                <h5 class="color font-weight-bold"> Especialidade </h5>
                                <hr></hr>
                                <div class="form-group">
                                    <label class="color" for="exampleInputEmail1">Área de atuação</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder=""></textarea>
                                </div>
                            </form>

                            <br></br>
                            <form>
                                <div class="form-group">
                                    <h5 class="color font-weight-bold"> Habilidades e competências </h5>
                                    <label class="color" for="exampleInputEmail1"> Conhecimentos do setor </label>
                                    <input type="" class="form-control" id="exampleInput" aria-describedby="emailHelp" placeholder="Ex.: UX,UI" />
                                    <br></br>

                                    <label class="color" for="exampleInputEmail1"> Ferramentas e Tecnologias </label>
                                    <input type="" class="form-control" id="exampleInput" aria-describedby="emailHelp" placeholder="Ex.: Github,Figma" />
                                    <br></br>

                                    <label class="color" for="exampleInputEmail1"> Competências Interpessoais </label>
                                    <input type="" class="form-control" id="exampleInput" aria-describedby="emailHelp" placeholder="Ex.: Proatividade, Pontualidade" />
                                    <br></br>

                                </div>
                            </form>

                            <br></br>
                            <div class="form-row">
                                <div class="col">
                                    <h5 class="color font-weight-bold"> Experiência Profissional</h5>
                                    <hr></hr>
                                    <label class="color" for="exampleInput"> Nome da empresa </label>
                                    <input type="" class="form-control" id="exampleInput" aria-describedby="emailHelp" />
                                </div>
                            </div>

                            <br></br>
                            <div class="form-row">
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Cargo ocupado</label>
                                    <input type="text" class="form-control" />
                                </div>
                                <br></br>
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Período</label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>

                            <br></br>
                            <form>
                                <div class="form-group">
                                    <label class="color" for="exampleInputEmail1"> Descrição </label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Quais foram as atividades que você exerceu durante o cargo?"></textarea>
                                </div>

                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1" />
                                    <nr></nr>
                                    <label class="custom-control-label color" for="customCheck1">NÃO POSSUO EXPERIÊNCIA </label>
                                    <button type="button" class="btn btn-outline-primary float-right"> <strong> + Adicionar </strong> </button>
                                </div>
                            </form>


                            <br></br> <br></br><br></br>
                            <h5 class="color font-weight-bold"> Cursos e Treinamentos </h5>
                            <hr></hr>
                            <div class="form-row">
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Nome do curso</label>
                                    <input type="text" class="form-control" />
                                </div>

                                <br></br>
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Nome da Instituição</label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                            <br></br>
                            <div class="form-row">
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Ano de conclusão</label>
                                    <input type="text" class="form-control" />
                                </div>
                                <br></br>
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Carga horária</label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>

                            <br></br>
                            <form>
                                <div class="form-group">
                                    <label class="color" for="exampleFormControlFile1"> Arquivo do certificado ou comprovante </label>
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1" />
                                    <br></br>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1" />
                                        <label class="custom-control-label color" for="customCheck1">NÃO POSSUO CURSOS </label>
                                        <button type="button" class="btn btn-outline-primary float-right"> <strong> + Adicionar </strong></button>
                                    </div>

                                </div>
                            </form>

                            <br></br><br></br>
                            <h5 class="color font-weight-bold"> Idiomas </h5>
                            <hr></hr>
                            <div class="form-row">
                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Idioma</label>
                                    <input type="text" class="form-control" />
                                </div>

                                <div class="col">
                                    <label class="color" for="exampleFormControlTextarea1">Nível</label>
                                    <input type="text" class="form-control" />
                                </div>

                            </div>
                                <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1" />
                                        <br></br>
                                        <label class="custom-control-label color" for="customCheck1">NÃO POSSUO CURSOS </label>
                                        <button type="button" class="btn btn-outline-primary float-right"><strong> + Adicionar </strong></button>
                                    </div>

                            <br></br><br></br>
                            <button type="button" class="btn btn-primary float-right">Salvar Currículo </button>
                        
                         </div>
                    </div>
                </div>
                <br></br><br></br>
            </div >

        )

    }
}

