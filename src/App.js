import React, { Component } from 'react'
import { BrowserRouter, Route } from 'react-router-dom';

import Footer from './footer/footer';
import Navbar from './commun/navbarInicio';
import NavbarCab from './commun/navbarCan';
import Login from './login/login';
import CadastroOpcao from './cadastroConta/cadastroOpcao';
import CadastroCandidato from './cadastroConta/cadastroCandidato';
import CadastroEmpresa from './cadastroConta/cadastroEmpresa';
import CadastroEmpresa2 from './cadastroConta/cadastroEmpresa2';
import Home from './home/telaInicial';
import PublicarVaga from './publicarVaga/publicarVaga';
import ListaDeCandidatos from './listaDeCandidatos/listaDeCandidatos';
import CadastroCurriculo from './cadastroCurriculo/cadastroCurriculo';
import Curriculo from './listaDeCandidatos/verCurriculo';
import VagasDisponiveis from './vagasDisponiveis/vagasDisponiveis';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: false
    };
  }
  render() {

    let logar = (tipo) => {
      if (this.state.login == false) {
        this.setState({
          login: true,
        })
      } else {
        this.setState({
          login: false,
        })
      }
    }

    return (
      <div>
        <BrowserRouter>
          {this.state.login ? <NavbarCab></NavbarCab> : <Navbar></Navbar>}

          <Login></Login>
          
          <CadastroOpcao></CadastroOpcao>
          <CadastroCandidato fun={logar}></CadastroCandidato>
          <CadastroEmpresa></CadastroEmpresa>
          <CadastroEmpresa2 fun={logar}></CadastroEmpresa2>
          <Curriculo></Curriculo>

          <Route exact path="/">
            <Home></Home>
          </Route>

          <Route path="/publicarvaga">
            <PublicarVaga></PublicarVaga>
          </Route>

          <Route path="/cadastrarcurriculo">
            <CadastroCurriculo></CadastroCurriculo>
          </Route>


          <Route path="/listadecandidatos">
            <ListaDeCandidatos></ListaDeCandidatos>
          </Route>

          <Route path="/vagasdisponiveis">
            <VagasDisponiveis></VagasDisponiveis>
          </Route>

          <Footer></Footer>

        </BrowserRouter>



      </div>
    );
  }
}

export default App;