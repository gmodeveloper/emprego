import React from 'react';
import Logo from '../img/logo-icon.png';

export default class Login extends React.Component {
    render() {
        return (
            <div class="modal fade" id="Login" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                <img class="img-fluid" src={Logo} alt=""></img>
                <br></br>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                <h5 class="modal-title" id="exampleModalLabel"> Bem-Vindo a Empregô!</h5>
                <br/>
                <div class="form-group">
                  <label for="usr">Email</label>
                  <input type="text" class="form-control" id="usr" name="username"/>
                </div>
                <div class="form-group">
                  <label for="pwd">Senha</label>
                  <input type="password" class="form-control" id="pwd" name="password"/>
                </div>
                      <a class="nav-linkSenha active" href="">Esqueceu a senha?</a>
                </div>
                <div class="modal-footer justify-content-center">
                  <button type="button" class="btn btn-primary">Entrar</button>
                </div>
                <button type="button" class="btn btn-link justify-content-center" data-toggle="modal" data-target="#cadastroOpcao" data-dismiss="modal" >Ainda não tem uma conta? Clique aqui!</button>
              </div>
            </div>
          </div>
        )
    }
}